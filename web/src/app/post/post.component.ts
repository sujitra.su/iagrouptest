import { Component, OnInit } from '@angular/core';
import { PostService } from './service/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  constructor(private postService: PostService) {}

  posts: Array<Post>;

  ngOnInit(): void {
    this.getPost();
  }

  getPost(): void {
    this.postService.getPost().subscribe((res) => {
      this.posts = res.result;
    });
  }
}

export class Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}