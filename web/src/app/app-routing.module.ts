import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CitizenComponent } from './citizen/citizen.component';
import { HeroComponent } from './hero/hero.component';
import { PostComponent } from './post/post.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'hero',
  },
  {
    path: 'hero',
    component: HeroComponent,
  },
  {
    path: 'posts',
    component: PostComponent,
  },
  {
    path: 'citizen',
    component: CitizenComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
