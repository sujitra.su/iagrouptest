import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CitizenService } from './service/citizen.service';

@Component({
  selector: 'app-citizen',
  templateUrl: './citizen.component.html',
  styleUrls: ['./citizen.component.scss'],
})
export class CitizenComponent implements OnInit {
  constructor(public fb: FormBuilder, private citizenService: CitizenService) {}

  input: string;
  result: any;
  citizenForm = this.fb.group({
    citizen: [''],
  });

  get myForm() {
    return this.citizenForm.get('citizen');
  }

  ngOnInit(): void {}

  onSubmit(): void {
    
    if (this.citizenForm.controls.citizen.value) {
      this.input = this.citizenForm.controls.citizen.value;
      var splitted = this.input.split('', 13);
      var citizen_id = splitted.toString();
    }
    this.citizenService.checkCitizen(citizen_id).subscribe((res) => {
      this.result = res;
    });
  }
}
