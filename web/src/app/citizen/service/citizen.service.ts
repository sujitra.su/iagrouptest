import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CitizenService {

  constructor(private http: HttpClient) { }

  public checkCitizen(id?: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/citizen/${id}/check-citizen`);
  }
}
