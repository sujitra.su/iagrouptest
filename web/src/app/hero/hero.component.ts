import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss'],
})
export class HeroComponent implements OnInit {
  heros: Hero[] = [
    { order: 11, name: 'Dr Nice' },
    { order: 12, name: 'necromancer' },
    { order: 13, name: 'Bombasto' },
    { order: 14, name: 'Celeritas' },
    { order: 15, name: 'Magnata' },
    { order: 16, name: 'RubberMan' },
    { order: 17, name: 'Dynama' },
    { order: 18, name: 'Dr IQ' },
    { order: 19, name: 'Magma' },
    { order: 20, name: 'Tornado' },
  ];

  result: string;
  constructor(public fb: FormBuilder) {}

  heroform = this.fb.group({
    name: [''],
  });

  get myForm() {
    return this.heroform.get('name');
  }

  ngOnInit(): void {}

  changeHero() {
    this.result = this.heroform.controls.name.value;
  }
}

export class Hero {
  order: number;
  name: string;
}
