﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Models
{
    public class Response
    {
        public bool success { get; set; }
        public string error_code { get; set; }
        public string error_msg { get; set; }
    }
}
