﻿
using api.Models;

namespace api.Service
{
    public interface ICitizenService
    {
        Response CheckCitizenId(string input);
    }
}
