﻿using api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Service
{
    public interface IPostService
    {
        Task<List<PostModel>> GetPostAsync();
    }
}
