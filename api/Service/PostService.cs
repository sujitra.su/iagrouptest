﻿using api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace api.Service
{
    public class PostService : IPostService
    {
        public async Task<List<PostModel>> GetPostAsync()
        {
            HttpClient client = new HttpClient();
            List<PostModel> Lst = new List<PostModel>();

            string path = "https://jsonplaceholder.typicode.com/posts";
            HttpResponseMessage response = client.GetAsync(path).Result;

            string responseJSON = await response.Content.ReadAsStringAsync();
            dynamic postData = JsonConvert.DeserializeObject(responseJSON);

            foreach (var p in postData)
            {
                PostModel model = new PostModel();
                model.UserId = p.userId;
                model.Id = p.id;
                model.Title = p.title;
                model.Body = p.body;
                Lst.Add(model);
            }
            return Lst;
        }
    }
}
