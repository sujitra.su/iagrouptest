﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Service
{
    public interface IAreaService
    {
        double CalculateTriangle(int Base, int Hight);
    }
}
