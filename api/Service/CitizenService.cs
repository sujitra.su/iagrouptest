﻿using api.Models;
using System;

namespace api.Service
{
    public class CitizenService : ICitizenService
    {
        public Response CheckCitizenId(string input)
        {
            var total = 0;
            var splitted = input.Split(',');
            var lastInput = splitted[splitted.Length - 1];
            for (var i = splitted.Length; i > 1; i--)
            {
                var index = splitted.Length - i;
                var number = Convert.ToInt32(splitted[index]);
                var sum = i * number;
                total += sum;
            }

            var mod = total % 11;
            var digit = 11 - mod;
            var lastDigit = digit.ToString().Split("", digit.ToString().Length);

            if (lastDigit[0] == lastInput.ToString())
            {
                Response res = new Response()
                {
                    success = true,
                    error_code = "200",
                    error_msg = ""
                };
                return res;
            }
            else if (lastDigit[0] != lastInput.ToString() && lastInput.ToString() != "undefined")
            {
                Response res = new Response()
                {
                    success = false,
                    error_code = "001",
                    error_msg = "citizen_id invalid"
                };
                return res;
            }
            else
            {
                Response res = new Response()
                {
                    success = false,
                    error_code = "001",
                    error_msg = "citizen_id require"
                };
                return res;
            }
        }
    }
}
